#!/bin/sh

DIR="$(dirname $0)"
SRC="$DIR/$(echo "$2" | sed 's|^\./||' | sed 's|/$||')"
OUT="$DIR/$(echo "$3" | sed 's|^\./||' | sed 's|/$||')"

MD5_FILE="$DIR/md5.sum"
LOG_FILE="$DIR/latex.log"

LATEX="/usr/local/texlive/2022/bin/x86_64-linux/pdflatex"

recompile_file()
{
  echo "\033[0;32mCompiling $(basename "$1")\033[0m"
  $LATEX -interaction=nonstopmode --shell-escape -output-directory "$(readlink -f "$(dirname "$1")" | sed "s|$SRC|$OUT|")" "$1" > "$LOG_FILE"
  if test "$?" != 0; then
    echo "\033[0;31mCompile Failed\033[0m"
    cat "$LOG_FILE"
  else
    echo "\033[0;32mCompile Completed\033[0m"
  fi
}

refresh_file()
{
  # Check if the file exists and is readable
  test ! -r "$1" && return;
  
  echo "\033[0;32mRefreshing $(basename "$1")\033[0m"
  
  # Fetch old sum if exists, and generate new sum alongside it
  OLD_MD5="$(grep -s "$1" "$MD5_FILE" | cut -d " " -f 1)"
  NEW_MD5="$(md5sum "$1" | cut -d " " -f 1)"
  
  # If file contents don't match, recompile
  if test "$OLD_MD5" != "$NEW_MD5"; then
    recompile_file "$1"
    # If there is no old sum, add new one to collection, else replace old sum
    if test -z "$OLD_MD5"; then
      echo "$NEW_MD5 $1" >> "$MD5_FILE"
    else
      sed "s|^.*$OLD_MD5\\s$1.*$|$NEW_MD5 $1|" "$MD5_FILE" -i
    fi
  fi
}

IFS=""

if test "$1" = "watch"; then
  inotifywait -m --include "\.tex" -r "$SRC" -e close_write --format "%w%f" |
  while read file; do
    refresh_file "$file"
  done
elif test "$1" = "recompile"; then
  find "$SRC" -iname "*.tex" |
  while read file; do
    recompile_file "$file"
  done
  echo "\033[0;32mDone!\033[0m"
elif test "$1" = "refresh"; then
  find "$SRC" -iname "*.tex" |
  while read file; do
    refresh_file "$file"
  done
  echo "\033[0;32mDone!\033[0m"
else
  printf "%s"\
'Usage: ./latexcompile.sh COMMAND SRC_DIR OUT_DIR
    watch     - watch for changes in a directory
    recompile - recompile all found .tex files
    refresh   - recompile all found .tex files with changes
'
fi
