\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{caption}

\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\newcommand\arcbetweennodes[3]{
    \pgfmathanglebetweenpoints{\pgfpointanchor{#1}{center}}{\pgfpointanchor{#2}{center}}
    \let#3\pgfmathresult}

\newcommand\arclabel[6][red,-stealth,shorten <=1pt,shorten >=1pt]{%
    \chemmove{
        \arcbetweennodes{#4}{#5}\angleend
        \arcbetweennodes{#4}{#3}\anglestart
        \ifdim\anglestart pt>\angleend pt \pgfmathsetmacro\anglestart{\anglestart-360}\fi
        \draw[#1]([shift=(\anglestart:#2)]#4)arc[start angle=\anglestart,end angle=\angleend,radius=#2];
        \pgfmathsetmacro\anglestart{(\anglestart+\angleend)/2}%
        \node[shift=(\anglestart:#2+1pt)#4,anchor=\anglestart+180,inner sep=0pt,outer sep=5pt]at(#4){#6};
    }
}

% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\begin{document}

% -------------------------------------------------------------------
\section{Bonding}
% -------------------------------------------------------------------

% -------------------------------------------------------------------
\subsection{Ionic Bonding}
% -------------------------------------------------------------------

Ions are atoms with a charge based on the proportion of electrons to
protons.

\begin{figure}[h]
\centering
\begin{tikzpicture}
  \node (li_atom) at (-7em, 0em) {Li};
  \draw[opacity=.25] (li_atom) circle (1em);
  \draw[black!75] (li_atom) circle (2em);
  \draw[black!75] (li_atom) circle (3em);
  \foreach \angle in {0, 180} {
    \fill[black!50] (li_atom) ++(\angle:2em) circle (3pt);
  }
  \fill[black!50] (li_atom) ++(180:3em) circle (3pt);
  
  \draw[-latex] (-3em, 0em) -- (3em, 0em)
    node[above, midway] {\large$-e^-$};
  
  \node (li_ion) at (8em, 0em) {Li};
  \draw[opacity=.25] (li_ion) circle (1em);
  \draw[black!75] (li_ion) circle (2em);
  \draw[black!75] (li_ion) circle (3em);
  \foreach \angle in {0, 180} {
    \fill[black!50] (li_ion) ++(\angle:2em) circle (3pt);
  }
  \draw [draw=black] (4em, -4em) -- (5em, -4em);
  \draw [draw=black] (4em, -4em) -- (4em,  4em);
  \draw [draw=black] (4em,  4em) -- (5em,  4em);
  \draw [draw=black] (12em, -4em) -- (11em, -4em);
  \draw [draw=black] (12em, -4em) -- (12em,  4em)
    node[right] {\large$+$};
  \draw [draw=black] (12em,  4em) -- (11em,  4em);
\end{tikzpicture}
\caption*{
Above is the element lithium, which has 3 electrons, 1 in
it's outer shell. If we remove that electron, the whole ion will
become positively charged.
}
\end{figure}

Charged ions attract and come together through electrostatic
attraction, and form what we consider to be a single compound. Most
salts and metal on non-metal compounds are bonded ionically.

Half equations are used to show the movement of electrons in an
ionic reaction.

\begin{center}
The half equations for \ce{4Li + O_2 -> 2Li_{2}O}\\
\large{
\ce{Li -> Li^+ + e^-}\\
\ce{O_2 + 4e^- -> 2O^{2-}}
}
\end{center}

% -------------------------------------------------------------------
\subsection{Covalent Bonding}
% -------------------------------------------------------------------

Covalent bonding is a type of chemical bonding which involves the
sharing of electrons between component atoms. These electrons are
known as shared pairs, and the electrostatic forces between the pairs
and the parent nuclei hold the molecule together. Covalent molecules
can bond in a way that creates an ion, which can then bond with other
ions through ionic bonding.

\begin{figure}[h]
\centering
\begin{tikzpicture}
  \node (n1) at (-3.2em, 0em) {N};
  \draw[opacity=.25] (n1) circle (1em);
  \draw[black!75] (n1) circle (2em);
  \draw[black!75] (n1) circle (3em);
  \foreach \angle in {0, 180} {
    \fill[black!50] (n1) ++(\angle:2em) circle (3pt);
  }
  \foreach \angle in {185, 175, 10, 0, 350} {
    \fill[black!50] (n1) ++(\angle:3em) circle (3pt);
  }
  \node (n2) at (3.2em, 0em) {N};
  \draw[opacity=.25] (n2) circle (1em);
  \draw[black!75] (n2) circle (2em);
  \draw[black!75] (n2) circle (3em);
  \foreach \angle in {0, 180} {
    \fill[black!50] (n2) ++(\angle:2em) circle (3pt);
  }
  \foreach \angle in {5, 355, 170, 180, 190} {
    \fill[black!50] (n2) ++(\angle:3em) circle (3pt);
  }
  \draw[red, line width=0.5mm] (0.6em, 0.9em) rectangle
  (-0.6em, -0.9em);
\end{tikzpicture}
\caption*{
Most of the gaseous non-metals (not including the noble gases) form
covalent diatomic molecules when pure. Above is \ce{N\bond{3}N} or
\ce{N_2}, which forms the second strongest bond possible in a
diatomic molecule.
}
\end{figure}

% -------------------------------------------------------------------
\subsection{Coordinate Bonding}
% -------------------------------------------------------------------

Coordinate bonds (also known as dative bonds) are a type of covalent
bond where both of the electrons originate from a single 'donor'
atom. Often, coordinately bonded molecules form ions, because of the
inbalance of electrons to protons.

\begin{figure}[h]
\centering
\chemfig{
  Al?
  (-[:135]\charge{[.radius=.5pt]45=\:, 225=\:, 135=\:}{Cl})
  (-[:225]\charge{[.radius=.5pt]135=\:, 225=\:, 315=\:}{Cl})
  -[:315]\charge{[.radius=.5pt]225=\:, 315=\:}{Cl}
  -[:45,,,,{-latex}]Al
  (-[:45]\charge{[.radius=.5pt]135=\:, 45=\:, 315=\:}{Cl})
  (-[:315]\charge{[.radius=.5pt]45=\:, 225=\:, 315=\:}{Cl})
  -[:135]\charge{[.radius=.5pt]45=\:, 135=\:}{Cl}?[,,,{latex-}]
\setcharge{[.radius=.5pt]}
}
\caption*{The Lewis diagram for the compound \ce{Al_2Cl_6}}
\end{figure}

% -------------------------------------------------------------------
\subsection{Metallic Bonding}
% -------------------------------------------------------------------

Metals are very large lattices of metal ions arranged in a tightly
packed grid. They are conductive because the lattice is filled with
delocalised electrons, which can move freely through the structure.
This allows the metals to conduct both electricity energy.
Metals can also conduct energy, because the electrons can hold some
thermal energy, which they can transfer throughout the structure.

Ions in a lattice are not bonded to a specific electron as such,
rather to the sea of delocalised electrons as a whole. This allows
individual atoms can slip over each other in a process called a
dislocation. While these dislocations happen atom-by-atom, in a
single crystal, the many millions of dislocations at once can create
a very large effect.

Metals have a high melting point because the amount of energy holding
the lattice together is very high. To overcome that energy, and allow
the metal atoms to move freely in a liquid state, alot of energy is
required, hence the high melting points of metallic materials.

The greater the charge of individual ions in the lattice, the greater
the attraction to the delocalised electrons and the greater the
strength of the metal.

% -------------------------------------------------------------------
\section{Molecular Structure}
% -------------------------------------------------------------------

Electrons in orbitals form pairs. Because of the similar
electrostatic charge, these areas of electron density repel eachother
to get as far apart as possible. This repulsion denotes the exact
angle at which a bond is formed. Bond angles are important to know,
since they dictate the overall structure of a molecule.

\vspace{5mm}

% -------------------------------------------------------------------
\subsection{Linear}
% -------------------------------------------------------------------

\begin{center}
\centering
\chemfig{@{a}Cl-@{b}\charge{[.radius=.5pt, extra sep=4pt]90=\:,
270=\:}{Be}-@{c}Cl}
\arclabel{0.5cm}{c}{b}{a}{\footnotesize$180^\circ$}

Linear molecules have a $180^\circ$ bond angle.
\end{center}

% -------------------------------------------------------------------
\subsection{Trigonal Planar}
% -------------------------------------------------------------------

\begin{center}
\chemfig{@{a}C(-[:120]@{b}H)(-[:240]H)=@{c}O}
\arclabel{0.5cm}{c}{a}{b}{\footnotesize $120^\circ$}

Trigonal planar molecules have a $120^\circ$ bond angle.
\end{center}

% -------------------------------------------------------------------
\subsection{Tetrahedral}
% -------------------------------------------------------------------

\begin{center}
\chemfig{@{a}C(-[:90]@{b}H)(<:[:210]H)(<[:270]H)-[:330]@{c}H}
\arclabel{0.5cm}{c}{a}{b}{\footnotesize $109.5^\circ$}

Tetrahedral molecules have a $109.5^\circ$ bond angle.
\end{center}

% -------------------------------------------------------------------
\subsection{Square Planar}
% -------------------------------------------------------------------

\begin{center}
\chemfig{@{a}Pt(<:[:45]@{b}NH_3)(<:[:135]@{c}Cl)(<[:225]Cl)<[:315]
NH_3}
\arclabel{0.5cm}{b}{a}{c}{\footnotesize $90^\circ$}

Square planar molecules have a $90^\circ$ bond angle.
\end{center}

% -------------------------------------------------------------------
\subsection{Trigonal Bipyramid}
% -------------------------------------------------------------------

\begin{center}
\chemfig{@{a}P(-[:180]@{b}Cl)(-[:90]@{c}Cl)(-[:270]Cl)(<[:330]@{d}Cl)
<:[:30]@{e}Cl}
\arclabel{0.5cm}{d}{a}{e}{\footnotesize $120^\circ$}
\arclabel{0.5cm}{c}{a}{b}{\footnotesize $90^\circ$}

Trigonal bipyramidal molecules have $120^\circ$ bond angles in one
plane, with the remaining 2 bonds in the purpendicular plane
\end{center}

% -------------------------------------------------------------------
\subsection{Octahedral}
% -------------------------------------------------------------------

\begin{center}
\chemfig{F>:[:210]@{a}S(<[:210]@{b}F)(<:[:150]@{c}F)(-[:270]F)(-[:90]
F)<[:330]F}
\arclabel{0.5cm}{c}{a}{b}{\footnotesize $90^\circ$}

Octahedral molecules have a $90^\circ$ bond angle.
\end{center}

% -------------------------------------------------------------------
\subsection{Triangular Pyramid}
% -------------------------------------------------------------------

\begin{center}
\chemfig{@{a}\charge{[.radius=.5pt, extra sep=4pt]90=\:}{N}(<:[:215]H
)(<[:270]@{b}H)-[:325]@{c}H}
\arclabel{0.5cm}{b}{a}{c}{\footnotesize $107^\circ$}

Octahedral molecules have a $90^\circ$ bond angle.
\end{center}

% -------------------------------------------------------------------
\subsection{Bent}
% -------------------------------------------------------------------

\begin{center}
\chemfig{@{a}\charge{[.radius=.5pt]45=\:, 135=\:}{O}
(<[:215]@{b}H)-[:325]@{c}H}
\arclabel{0.5cm}{b}{a}{c}{\footnotesize $105.5^\circ$}

Octahedral molecules have a $90^\circ$ bond angle.
\end{center}
\vspace{5mm}

\newpage
% -------------------------------------------------------------------
\section{Intermolecular Forces}
% -------------------------------------------------------------------
% -------------------------------------------------------------------
\subsection{Van der Waals}
% -------------------------------------------------------------------

\begin{figure}[h]
\centering
\begin{tikzpicture}
  \node (n1) at (4em, 0em) {Ar};
  \draw[black!75] (n1) circle (3em);
  \foreach \angle in {30, 15, 0, 345, 330} {
    \draw (n1) ++(\angle:2.6em) node {\footnotesize$-$};
  }
  \foreach \angle in {150, 165, 180, 195, 210} {
    \draw (n1) ++(\angle:2.6em) node {\footnotesize$+$};
  }
  \node (n2) at (-4em, 0em) {Ar};
  \draw[black!75] (n2) circle (3em);
  \draw[black!75] (n1) circle (3em);
  \foreach \angle in {30, 15, 0, 345, 330} {
    \draw (n2) ++(\angle:2.6em) node {\footnotesize$-$};
  }
  \foreach \angle in {150, 165, 180, 195, 210} {
    \draw (n2) ++(\angle:2.6em) node {\footnotesize$+$};
  }
  \foreach \x in {-0.6, -0.3, 0.0, 0.3, 0.6} {
    \draw[line width=1pt] (\x em, 0.6em) -- (\x em, -0.6em);
  }
\end{tikzpicture}
\caption*{
Van der Waals forces are electrostatically induced forces between
indivudual atoms. Electrons exist randomly within electron orbitals,
which can occasionally cause a difference in the charge distribution.
This charge differential within an atom can induce opposing charges
in other atoms, causing them to attract.
}
\end{figure}

Van der Waals forces are particularly strong between long chain
molecules such as hydrocarbons, due to the large amount of surface
area on such a chain.

% -------------------------------------------------------------------
\subsection{Permanent Dipoles}
% -------------------------------------------------------------------

Permanent dipoles are molecules in which there is an unbalanced
distribution of charge. Such a charge imbalance is a result of a
difference in the electronegativity of constituent atoms or asymmetry
in a molecule's structure. The overall polarity of a molecule is a
result of of the dipole moment, or the direction of charge imbalance.

Take Carbon Tetrachloride (\ce{CCl_4}):
\begin{figure}[h]
\centering
\chemfig{C(-[:90]Cl)(<:[:210]Cl)(<[:270]Cl)-[:330]Cl}
\end{figure}

Since all 4 Chlorines have the same electronegativity, there is no
charge imbalance. This means it is non polar.

If we were to replace one of the carbons with a Hydrogen, we would
get Chloroform:
Which has a dipole moment in the direction of the 3 remaining
Chlorines, making it a polar molecule.

This difference in polarity is apparent in real life:
Carbon Tetrachloride has a soluablility of $0.0081 g/L$ at
$25^{\circ}C$, while Chloroform has a soluability of $10.1 g/L$, and
Dichloromethane has an even higher soluability of $17.5 g/L$, again
due to it's higher polarity.

\begin{figure}[h]
\centering
\vspace{1em}
\setchemfig{atom sep=2em}
\chemfig{
  C(-[:120]\chemabove{H}{\small\delta+})(-[:240]\chembelow{H}
  {\small\delta+})=\chemabove{O}{\small\delta-}
}

\vspace{2em}
There is a charge difference within a molecule based on the
electronegativity of the individual atoms. This charge difference is
shown by the deltas on the molecule above.
\end{figure}

% -------------------------------------------------------------------
\subsection{Hydrogen Bonding}
% -------------------------------------------------------------------

Hydrogen bonding is the strongest intermolecular force at about
$10 \times$ weaker than covalent bonding.

Hydrogen bonding is a type of Dipole-Dipole interaction, however,
it only involves Hydrogen and Nitrogen, Oxygen or Fluorine.

\begin{figure}[h]
\centering
\setchemfig{atom sep=2em}
\chemfig{
  H?-[:30]O(-[:90]H)>:[:330]H-[:270]O(-[:330]H)>:[:210]H-[:150]
  O?[,{<:}](-[:210]H)
}
\caption*{
When water solidifies, it forms hydrogen bonds between the hydrogen
and oxygen, which forces it into a hexagonal crystal. This crystal
structure is less dense than liquid water.
}
\end{figure}

\end{document}
