\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{caption}

\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\begin{document}

% -------------------------------------------------------------------
\section{What's so special about carbon?}
% -------------------------------------------------------------------

Carbon is such a unique element because it can form long chains molecules,
which also often branch off into other sections (called reactive groups).
This quality allows carbon to form large, complex molecules with other
elements, most commonly Hydrogen, Nitrogen and Oxygen.

\begin{figure}[h]
\centering
\chemname{
	\chemfig{O=[:270]-[:210]N(-[:150])-[:270](=[:210]O)-[:330]N(-[:270])-[:30]%
	-[:342.2,0.994]N=^[:54,0.994]-[:126,0.994]N(-[:71.9])-[:197.8,0.994](%
	=^[:270])(-[:150])}
}{Caffeine}
\caption*{
	Caffeine is a complex organic molecule, commonly used as a
	CNS stimulant. Carbon is at the heart of Caffeine, enabling it's
	complex structure.
}
\end{figure}

Carbon can do this because of it can create four, relatively strong
covalent bonds. Other elements in the 6th period have such as Silicon
and Germanium display similar properties in the sense that they can form
large lattices like Graphite, however their chemical bonds tend to be
weaker due to their atomic size.

% -------------------------------------------------------------------
\section{Organic Chemistry Basics}
% -------------------------------------------------------------------

% -------------------------------------------------------------------
\subsection{Skeletal and Molecular Formulae}
% -------------------------------------------------------------------

In organic chemistry, the physical structure of the molecule is quite
important, so organic chemists tend to use structural formulae when
drawing chemicals. However, structural formulae tend to complex and
difficult to interpret. For that reason, organic chemists often instead
use molecular formulae to show the number of atoms in the molecule.

\begin{figure}[h]
\centering
\chemnameinit{\chemfig{O=[:90](-[:150])-[:30]O-[:330]-[:30]}}
\chemname{
	\chemfig{O=[:90](-[:150])-[:30]O-[:330]-[:30]}
}{Ethyl Ethanoate}
\caption*{
	Ethyl Ethanoate's molecular formula would be:\\
	{\ce{CH_3CO_2CH_2CH_3}} or {\ce{C_4H_8O_2}}
}
\end{figure}

The structural formula of Ethyl Ethanoate shows the ester group very
clearly, while the molecular formula does not. However the molecular
formula makes the composition of the molecule very clear. Both
methods have their trade-offs, although for organic chemistry,
structural formulae tend to be preferred.

% -------------------------------------------------------------------
\subsection{Empirical Formulae}
% -------------------------------------------------------------------

In organic chemistry, empirical formulae is used to show the simplest
whole number ratio of atoms in a molecule.

\begin{figure}[h]
\centering
\chemnameinit{\chemfig{H-C(-[:90]H)(-[:-90]H)-C(-[:90]H)(-[:-90]H)-H}}
\chemname{
	\chemfig{H-C(-[:90]H)(-[:-90]H)-C(-[:90]H)(-[:-90]H)-H}
}{Ethane}
\end{figure}

Ethane's molecular formula is \ce{C_2H_6}, which puts the ratio of Carbon
to Hydrogen at $2:6$, or at its simplest, $1:3$. That means the empirical
formula of Ethane would be \ce{C_1H_3} or simply \ce{CH_3}

% -------------------------------------------------------------------
\subsection{Example Question}
% -------------------------------------------------------------------

A $1.50g$ sample of \ce{H_3Y.$x$H_2O} (Citric Acid) contains $0.913g$ Oxygen
by mass. The sample burns completely in air to form $1.89g$ \ce{CO_2} and
$0.643g$ \ce{H_2O}. Show that the empirical formula of Citric Acid is 
\ce{C_3H_5O_4}.

\paragraph{Working:}

\begin{center}
\ce{H_3Y.$x$H_2O + $c$O_2 -> $a$CO_2 + $b$H_2O}
\end{center}

Oxygen in \ce{H_3Y.$x$H_2O}: $\frac{0.912g}{16mol} = 0.057mol$\\
Carbon in \ce{CO_2}: $\frac{1.89g}{44mol} = 0.043mol$\\
Hydrogen in \ce{H_2O}: $\frac{0.643g}{18} = 0.0357 \times 2 = 0.0714mol$

Normalize everything relative to the lowest value:

Carbon: $\frac{0.043}{0.043} = 1$\\
Hydrogen: $\frac{0.0714}{0.043} = 1.66$\\
Oxygen: $\frac{0.057}{0.043} = 1.33$

$ = 3:5:4$

% -------------------------------------------------------------------
\paragraph{Empirical $\rightarrow$ Molecular:}
% -------------------------------------------------------------------

Find the molecular formula of \ce{CH_2O} when the mass is $180gmol^{-1}$

RAM of \ce{CH_2O}: 30\\
$\frac{180}{30} = 6$

The molecular formula must then be \ce{C_6H_{12}O_6}

\clearpage
% -------------------------------------------------------------------
\section{Nomanclature}
% -------------------------------------------------------------------

Chemists have devised a standard method for naming chemicals in a way
that makes them universal, and describes to an extent the properties
of the molecules.

% -------------------------------------------------------------------
\subsection{Roots}
% -------------------------------------------------------------------

In chemistry, all systematic names have a root, named after the
longest unbranched chain or ring.

For the naming, use:

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Length & Group & Root\\
\hline
1 & Methyl & Meth-\\
2 & Ethyl & Eth-\\
3 & Propyl & Prop-\\
4 & Butyl & But-\\
5 & Pentyl & Pent-\\
6 & Hexyl & Hex-\\
7 & Heptyl & Hept-\\
8 & Octyl & Oct-\\
9 & Nonyl & Non-\\
10 & Decyl & Dec-\\
\hline
\end{tabular}
\end{center}

\begin{figure}[h]
\centering
\chemname{
	\chemfig{-[:30]-[:330]-[:30]-[:330]-[:30]}
}{Hexane}
\caption*{
	The above diagram has a chain of 6 Carbons, so it must have a 'hexa-'
	root. The diagram lacks any \ce{C=C} double bonds, so it must also be
	an alkane. Therefore, it's systematic name must be hexane.
}
\end{figure}

% -------------------------------------------------------------------
\subsection{Branches}
% -------------------------------------------------------------------

Branches are added as a prefix of the root, in the form 
(branch group)(root).

The exact position of the branch on the root
chain is specified in the locant. Molecules with multiple branches
list them in alphabetical order, with the locant before each
individual branch.

\begin{figure}[h]
\centering
\chemnameinit{\chemfig{OH-[:150,,1]-[:210](-[:150])-[:270]}}
\chemname{
	\chemfig{OH-[:150,,1](-[6,0.2,,,draw=none]\scriptstyle1)-[:210]
	(-[6,-0.2,,,draw=none]\scriptstyle2)(-[:150]
	-[6,0.2,,,draw=none]\scriptstyle3
	)-[:270]}
}{2-methylpropan-1-ol}
\caption*{
	Here, the methyl group is bonded to the 2nd carbon
	(hence 2-methyl), and the alcohol group is bonded to the 1st
	carbon (hence propan-1-ol).
}
\end{figure}

% -------------------------------------------------------------------
\subsection{Cyclic Molecules}
% -------------------------------------------------------------------

Cyclic molecules, or molecules with cyclic submolecules, are simply
denoted with the prefix 'cyclo-'

\begin{figure}[h]
\centering
\chemnameinit{\chemfig{\chemfig{-[:216]-[:288]--[:72](-[:144])}}}
\chemname{
\chemfig{-[:216]-[:288]--[:72](-[:144])}
}{Cyclopentane}
\caption*{
	This alkane has a chain length of 5, which makes it pentane.
	Given that it is a cyclic molecule, it's systematic name would
	be cyclopentane.
}
\end{figure}

% -------------------------------------------------------------------
\subsection{Groups and Group Names}
% -------------------------------------------------------------------

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
Group & Formula & Suffix\\
\hline
alkanes & \ce{C_nH_{2n+2}} & -ane\\
alkenes & \ce{R-CH=CH-R} & -ene\\
halogenoalkanes &  & \\
alcohols &  & \\
aldehydes &  & \\
ketones &  & \\
carboxylic acids &  & \\
\hline
\end{tabular}
\end{center}

% -------------------------------------------------------------------
\section{Structural Isomers}
% -------------------------------------------------------------------

Isomers are molecules that have the same molecular formula but whose
atoms are arranged differently. There are two basic types of
isomerism in chemistry - Structural Isomerism and Stereoisomerism

Structural isomers are defined as having the same molecular formula, 
but different structural formulae. There are three sub-divisions:

1 - Positional Isomerism - The same functional groups attatched at 
different points along the chain

\begin{figure}[h]
\centering
\chemnameinit{\chemfig{-[:30]-[:330](-[:270]Cl)-[:30]}}
\chemname{
	\chemfig{-[:30]-[:330]-[:30]-[:330]Cl}
}{1-Chlorobutane}
\chemname{
	\chemfig{-[:30]-[:330](-[:270]Cl)-[:30]}
}{2-Chlorobutane}
\caption*{
	While both of these compounds have the same chemical formula,
	the position of the Chlorine groups along the chain are
	different, which can often give the compound different reactive
	properties.
}
\end{figure}

2 - Functional Group Isomerism - functional groups that are
different

3 - Chain Isomerism - The hydrocarbon chain is arranged differently

\end{document}
