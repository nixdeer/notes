\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{caption}
\usepackage{pgfplots}
\usepackage{pgfplotstable}

\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\begin{document}

% -------------------------------------------------------------------
\section{The Photoelectric Effect}
% -------------------------------------------------------------------

In 1887, Heinrich Hertz observed that exposing a spark gap to
electromagnetic radiation caused a spark to jump. Upon placing the
spark gap in a darkened box, he observed that the maximum spark
length was smaller. Hertz observed that the intensity of the light
had no effect on the maximum length of the spark and that the
frequency had the greatest effect.

In 1900, Max Planck suggested that energy in electromagnetic waves
could only be delivered in packets of energy, called photons, which
Albert Einstein later linked to the photoelectric effect.

Our current model of the photoelectric affect suggests that a
photoelectron can only be emitted from a surface if it is exposed to
a single photon of a equal or greater energy than the threshold
energy.

% -------------------------------------------------------------------
\subsection{The Modern Theory of the Photoelectric Effect}
% -------------------------------------------------------------------

The energy of a photon is defined as $hf$, where $h$ is Plank's
Constant $6.63\times10^{-34}Js$.

When a photon is incident to a surface, the electron at the surface
absorbs a \textbf{single} photon, and therefore gains that photon's
energy. If the gained energy exceeds the metal's
\textbf{work function} $\phi$, the photoelectron is instantaniously
emitted with a maximum energy equal to $E_{Kmax} = hf - \phi$.

Note that photoelectrons often don't leave the metal with an energy
equal to $E_{Kmax}$, since electrons released below the surface will
lose some energy to interactions as they make their way out.

The work function is the minimum energy required to release an
electron from the surface of a metal. The threshold frequency $f_{0}$
is the lowest frequency of incident light to release a photoelectron.
Above this frequency, the number of photoelectrons emitted per second
is proporional to the intensity of the incident radiation.

To find the work function of a metal, use $hf = \phi + E_{Kmax}$,
where $E_{Kmax} = 0$.\\
Knowing the threshold frequency, we can calculate the work function
using the equation $hf_{0} = \phi$

% -------------------------------------------------------------------
\subsection{Conduction Electrons}
% -------------------------------------------------------------------

When an electron gains less energy than the metal's work function, it
needs to lose that energy. The electron can either gain kinetic
energy and move around the metal, colliding with ions and electrons
to dissipate it's gained energy, or it can re-emit that photon at a
slighly lower frequency.

% -------------------------------------------------------------------
\subsection{The Potential Well Model}
% -------------------------------------------------------------------

The potential well model is a simple method of visualizing the
photoelectric effect. Consider that the electron is a cat stuck down
a well, and needs to gain at least the \textbf{work function} of the
well to escape.

\begin{figure}[h]
\centering
\begin{tikzpicture}
\draw (4,5) -- (1.5,5) -- (1.5,0) -- (-1.5,0) -- (-1.5,5) -- (-4,5);
\draw [latex-latex,blue] (1.8,4.8) -- (1.8,0.2)
	node [right,midway] {$\phi$};
\node [red,draw,circle] at (-0.8,0.7) {$e^{-}$};
\node [red,draw,circle] at (0.8,4) {$e^{-}$};
\draw [-latex] (0.8,0.3) -- (0.8,3.4)
	node [left,midway] {$E < \phi$};
\end{tikzpicture}
\caption*{
	Here the electron (our cat) has not gained enough kinetic energy
	to escape (i.e. has not jumped high enough).
}
\end{figure}

\begin{figure}[h]
\centering
\begin{tikzpicture}
\draw (4,5) -- (1.5,5) -- (1.5,0) -- (-1.5,0) -- (-1.5,5) -- (-4,5);
\draw [latex-latex,blue] (1.8,4.8) -- (1.8,0.2)
	node [right,midway] {$\phi$};
\node [red,draw,circle] at (-0.8,0.7) {$e^{-}$};
\node [red,draw,circle] at (0.8,8) {$e^{-}$};
\draw [-latex] (0.8,0.3) -- (0.8,7.4)
	node [left,midway] {$E \ge \phi$};
\draw [-latex] (1.8,5.3) -- (1.8,7.4)
	node [right,midway] {$E_{Kmax}$};
\end{tikzpicture}
\caption*{
	Here the electron (our cat) \textbf{has} gained enough energy to
	jump out the well, so escapes with a remaining kinetic energy
	equal to $E - \phi$.
}
\end{figure}

In the photoelectric effect the electron cannot gain energy from two
photons simultaniously, since that would be the equivalent of the cat
jumping with the energy of one photon, landing, then jumping again
with the energy of the next photon.

% -------------------------------------------------------------------
\subsection{Stopping Potential}
% -------------------------------------------------------------------

The stopping potential is the voltage required to stop electrons
reaching the opposite electrode. It is equal to the energy in
electronvolts ($1eV = 1.6\times10^{-19}$). It is equal to the maximum
kinetic energy the electrons gain.

\newpage
% -------------------------------------------------------------------
\subsection{Determination of Planck's constant}
% -------------------------------------------------------------------
\begin{figure}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
Wavelength ($nm$) & Frequency ($Hz$) & Stopping Voltage ($V_{s}$) & $V_{s}$ ($eV$) & Energy ($10^{-20}J$)\\
$620$             & 4.88             & 0.18                       & 0.18           & 2.88\\
$595$             & 5.04             & 0.34                       & 0.34           & 5.44\\
$550$             & 5.45             & 0.43                       & 0.43           & 6.88\\
$535$             & 5.61             & 0.47                       & 0.47           & 7.52\\
$495$             & 6.06             & 0.64                       & 0.64           & 10.24\\
$465$             & 6.45             & 0.78                       & 0.78           & 12.48\\
$415$             & 7.23             & 0.94                       & 0.94           & 15.04\\
\end{tabular}
\caption*{
	Milikan's experiment can be used to determine the maximum kinetic
	energy of the electrons. Light is shone through a filter onto a
	charged metal plate.
}
\end{figure}

\begin{figure}[h]
\centering
\pgfplotstableread{
X Y
4.88 2.88
5.04 5.44
5.45 6.88
5.61 7.52
6.06 10.24
6.45 12.48
7.23 15.04
}\milikan
\begin{tikzpicture}
\begin{axis}[
    xlabel={\scriptsize{Frequency ($10^{14}$)}},
    ylabel={\scriptsize{Energy ($10^{-20}$)}},
    xmin=4, xmax=9,
    ymin=0, ymax=16,
    xtick={4,5,6,7,8},
    ytick={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16},
	axis lines=left,
	width=15cm,height=10cm
]
\addplot [dashed, thick] table[y={create col/linear regression={y=Y}}]{\milikan};
\addplot [dashed, thick] {\pgfplotstableregressiona * x \pgfplotstableregressionb};
\addplot[color=red, mark=square] table {\milikan};
\addlegendentry{$\pgfmathprintnumber{\pgfplotstableregressiona} \cdot
x\pgfmathprintnumber[print sign]{\pgfplotstableregressionb}$};
\end{axis}
\end{tikzpicture}
\caption*{
	Notice that a graph of $E_{Kmax}$ to $f$ will result in a
	straight line where $m$ of the linear trend line is $h$ and $c$
	is $\phi$
	
	Therefore, $\phi$ is found by multiplying $c$ by the $y$ scale:
	$-20.51\cdot10^{-20}$ or $-2.051\cdot10^{-19}$.
}
\end{figure}

\end{document}

