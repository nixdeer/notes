\documentclass[a4paper, 12pt]{article}

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{tikz}
\usepackage{caption}

\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\begin{document}

\section{Waves}

Waves are special in that they transfer energy (and information)
without transferring matter.

\paragraph{Examples include:}
\begin{center}
\begin{tabular}{|c|c|}
Mechanical & Electromagnetic\\
Sound & Light (visible)\\
Seismic & Microwaves\\
Water & Radio\\
 & Gamma\\
 & Infra-red\\
 & Ultraviolet\\
 & X-Rays
\end{tabular}
\end{center}

Waves are produced by vibrations/oscillations which transfers energy
'into' the wave.

\section{Drawing Waves}

\subsection{Ray Diagrams}

Since waves often travel along a straight axis, we can draw them
simply as straight lines.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=1.4]
	\draw [dashed] (0, 2) -- (0, 0);
	\draw (0, 0) -- (2, 2);
	\draw (0.85, 1) -- (1, 1);
	\draw (1, 0.85) -- (1, 1);
	\draw (-2, 2) -- (0, 0);
	\draw (-1.15, 1) -- (-1, 1);
	\draw (-1, 1.15) -- (-1, 1);
	\draw (-2, 0) -- (2, 0);
	\foreach \i in {-2.0,-1.8,...,2.0}{
		\draw (\i, 0) -- (\i-0.2, -0.2);
	}
\end{tikzpicture}
\caption*{Example: Reflection of Light 'Rays'}
\end{figure}

\subsection{Wave Fronts}
Wave front diagrams show the waves as lines purpendicular to the
transmission direction, with the distance between the lines
representing the wavelength

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=1.4]
	\draw (0, 0) circle [radius=0.5cm];
	\draw (0, 0) circle [radius=1cm];
	\draw (0, 0) circle [radius=1.5cm];
	{ [red]
		\draw [latex-latex](-2, 0) -- (2, 0);
		\draw [latex-latex](0, -2) -- (0, 2);
		\draw [latex-latex](-1.5, 1.5) -- (1.5, -1.5);
		\draw [latex-latex](-1.5, -1.5) -- (1.5, 1.5);
	}
	\fill (0, 0) circle (2pt);
\end{tikzpicture}
\caption*{Example: Water Waves From a Point}
\end{figure}

Here, the waves are propagating from a single point, with the
direction of travel represented by the red arrows.

\subsection{Graphical}

Graphical Diagrams are pretty self explanatory; the displacement of
the wave is plotted as a function of time.

\begin{figure}[h]
\centering
\begin{tikzpicture}[font=\scriptsize]
	{ [gray]
		\draw (0,0) -- (pi*4,0);
		\draw (0,-1.2) -- (0,1.2);
	}
	\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(\x)});
	{ [red]
		\draw [latex-latex] (pi*0.5, 0) -- (pi*0.5, 1)
		node[above] {Amplitude};
		\draw [dashed] (pi, 0) -- (pi, -1.3);
		\draw [dashed] (pi*3, 0) -- (pi*3, -1.3);
		\draw [latex-latex] (pi, -1.2) -- (pi*3, -1.2)
		node[below,align=center,midway] {Wavelength $\lambda$};
	}
\end{tikzpicture}
\end{figure}

Time Period - The time for one oscillation to occur.\\
Frequency - The number of oscillations in a second.\\
Amplitude - The maximum displacement from the axis.\\
Wavelength - The length in meters of one oscillation of the wave.

\section{The Wave Equation}
\begin{center}
$wave speed = frequency \times wavelength$\\
$c = f \times \lambda$

$speed = \frac{distance}{time}$,
for waves: $speed = \frac{wavelength}{time period}$,\\
but $\frac{1}{T} = f$\\ so $c = f \times \lambda$
\end{center}

\section{Wave Properties}
\subsection{Path and Phase Difference}
\begin{figure}[h]
\centering
\begin{tikzpicture}[font=\scriptsize]
    \draw (0,0) -- (pi*2,0);
    \foreach \deg in {90, 180, 270, 360}
    	\draw (pi/180*\deg,2pt) -- (pi/180*\deg,-2pt) node[below] {$\deg^\circ$};
    
	\draw (0,-1.2) -- (0,1.2);
	\foreach \y in {-1,1}
    	\draw (2pt,\y) -- (-2pt,\y) node[left] {$\y$};
    
	\draw [red] plot[smooth,domain=0:360] (pi/180*\x,{sin(\x)});
	\draw [blue] plot[smooth,domain=0:360] (pi/180*\x,{cos(\x)});
	{ [red]
		\draw (pi, -1.1) -- (pi, -1.3);
		\draw (pi*1.5, -1.1) -- (pi*1.5, -1.3);
		\draw [latex-latex] (pi, -1.2) -- (pi*1.5, -1.2)
		node[below,align=center,midway] {Phase Difference};
	}
\end{tikzpicture}
\caption*{
The phase difference is the number of degrees difference between two
waves, wheras the path difference is the physical distance between
two points on a wave.
}
\end{figure}

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.8]
    \draw (0,0) -- (pi*2,0);
    \foreach \deg in {90, 180, 270, 360}
    	\draw (pi/180*\deg,2pt) -- (pi/180*\deg,-2pt) node[below] {$\deg^\circ$};
    
	\draw (0,-1.2) -- (0,1.2);
	\foreach \y in {-1,1}
    	\draw (2pt,\y) -- (-2pt,\y) node[left] {$\y$};
	\draw [red] plot[smooth,domain=0:360] (pi/180*\x, {sin(\x)});
	\draw [blue] plot[smooth,domain=0:360] (pi/180*\x+0.05, {sin(\x)});
\end{tikzpicture}
\caption{Two waves directly in phase.}
\end{figure}

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.8]
    \draw (0,0) -- (pi*2,0);
    \foreach \deg in {90, 180, 270, 360}
    	\draw (pi/180*\deg,2pt) -- (pi/180*\deg,-2pt) node[below] {$\deg^\circ$};
    
	\draw (0,-1.2) -- (0,1.2);
	\foreach \y in {-1,1}
    	\draw (2pt,\y) -- (-2pt,\y) node[left] {$\y$};
	\draw [red] plot[smooth,domain=0:360] (pi/180*\x, {sin(\x)});
	\draw [blue] plot[smooth,domain=0:360] (pi/180*\x, {sin(-\x)});
\end{tikzpicture}
\caption{Two waves 180$^\circ$ out of phase.}
\end{figure}

\end{document}

