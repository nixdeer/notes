\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{caption}

\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\begin{document}

% -------------------------------------------------------------------
\section{Principle of Superposition}
% -------------------------------------------------------------------

When two or more waves interact, the resulting displacement is the
sum of the displacements of the individual waves.

\begin{figure}[h]
\centering
\begin{tikzpicture}
\draw (0, -2) -- (0, 2)
  node [above] {$x$};
\draw (0, 0) -- (pi*4, 0)
  node [right] {$y$};
\draw [dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x)});
\draw [dashed,blue] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x*2)});
\draw [red] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x*2)+sin(\x)});
\end{tikzpicture}
\caption*{
The principle of superposition describes how waves sum as they
interact, causing interference.
}
\end{figure}

When similar waves interact, they can form what we call an
\textbf{interference pattern}.

% -------------------------------------------------------------------
\subsection{Constructive Interference}
% -------------------------------------------------------------------

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.5]
\draw (0, -2) -- (0, 2)
  node [above] {$x$};
\draw (0, 0) -- (pi*4, 0)
  node [right] {$y$};
\draw [dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x) + 0.05});
\draw [blue, dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x) - 0.05});
\draw [red] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x) + sin(\x)});
\end{tikzpicture}
\end{figure}

When two waves directly in phase with each other overlap, their
oscillations add and the amplitude of the wave increases to the sum
of both waves' amplitude.

% -------------------------------------------------------------------
\subsection{Destructive Interference}
% -------------------------------------------------------------------

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.5]
\draw (0, -2) -- (0, 2)
  node [above] {$x$};
\draw (0, 0) -- (pi*4, 0)
  node [right] {$y$};
\draw [dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x)});
\draw [blue, dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x + 180)});
\draw [red] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x) + sin(\x + 180)});
\end{tikzpicture}
\end{figure}

When two waves directly out of phase with each other overlap, their
oscillations also add, but the peak of one wave adds to the trough of
the other to make a total displacement of none.

\newpage
% -------------------------------------------------------------------
\section{Stationary Waves}
% -------------------------------------------------------------------

A stationary wave is produced when two waves of equal frequency and
amplitude travel in opposite directions and \textbf{superpose}.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.5]
\draw [red] (3*pi,1) rectangle (4*pi, -1);
\draw [dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x)});
\draw [blue, dashed] plot[smooth,domain=0:720] (pi/180*\x,{sin(\x + 180)});
\foreach \x in {0, 1, 2, 3, 4} {
  \node at (pi*\x, -1) {n};
}
\foreach \x in {1, 3, 5, 7} {
  \node at (\x*pi/2, 1.5) {a};
}
\end{tikzpicture}
\caption*{
  At the nodes (n), the displacement is 0. At the anti-nodes (a), the
  displacement changes from the maximum positive to maximum negative.

  At the nodes there is destructive interference, while at the
  anti-nodes there is \textbf{both constructive and destructive}
  interference.

  In a progressive wave, each point has a different amplitude to it's
  neighbor.

  The section highlighted in red is a single loop.
}
\end{figure}

Stationary waves are commonly produced when a wave reflects off a
surface. When waves reflect, they are re-emitted $180^\circ$ out of
phase.

In any loop, two different points are in phase with each other.
Between adjacent loops, the phase difference is $180^\circ$

% -------------------------------------------------------------------
\subsection{Stationary Waves on a string}
% -------------------------------------------------------------------

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.5]
\draw [latex-latex] (0, 1.5) -- (4*pi, 1.5)
  node [above, midway] {$l$};
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(1/4*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(1/4*\x)});
\node at (4*pi, 0) [right] {First harmonic $f_0$};
\node at (0, 0) [left] {$l=\frac{\lambda}{2}$};
\end{tikzpicture}
\vspace{2em}

\begin{tikzpicture}[scale=0.5]
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(1/2*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(1/2*\x)});
\node at (4*pi, 0) [right] {Second harmonic $2f_0$};
\node at (0, 0) [left] {$l=\lambda$};
\end{tikzpicture}
\vspace{2em}

\begin{tikzpicture}[scale=0.5]
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(3/4*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(3/4*\x)});
\node at (4*pi, 0) [right] {Third harmonic $3f_0$};
\node at (0, 0) [left] {$l=\frac{3\lambda}{2}$};
\end{tikzpicture}
\caption*{\LARGE...}
\end{figure}

% -------------------------------------------------------------------
\subsection{Stationary Waves in a pipe}
% -------------------------------------------------------------------

\paragraph{Closed}
\begin{center}
\begin{tikzpicture}[scale=0.5]
\draw [latex-latex] (0, 1.5) -- (4*pi, 1.5)
  node [above, midway] {$l$};
\draw (4*pi, -1) -- (0, -1) -- (0, 1) -- (4*pi, 1);
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(1/8*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(1/8*\x)});
\node at (4*pi, 0) [right] {First harmonic $f_0$};
\node at (0, 0) [left] {$l=\frac{\lambda}{4}$};
\end{tikzpicture}

\begin{tikzpicture}[scale=0.5]
\draw (4*pi, -1) -- (0, -1) -- (0, 1) -- (4*pi, 1);
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(3/8*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(3/8*\x)});
\node at (4*pi, 0) [right] {Second harmonic $3f_0$};
\node at (0, 0) [left] {$l=\frac{3\lambda}{4}$};
\end{tikzpicture}

\begin{tikzpicture}[scale=0.5]
\draw (4*pi, -1) -- (0, -1) -- (0, 1) -- (4*pi, 1);
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(5/8*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(5/8*\x)});
\node at (4*pi, 0) [right] {Third harmonic $5f_0$};
\node at (0, 0) [left] {$l=\frac{5\lambda}{4}$};
\end{tikzpicture}
\end{center}

\paragraph{Open}
\begin{center}
\begin{tikzpicture}[scale=0.5]
\draw [latex-latex] (0, 1.5) -- (4*pi, 1.5)
  node [above, midway] {$l$};
\draw (4*pi, -1) -- (0, -1);
\draw (0, 1) -- (4*pi, 1);
\draw plot[smooth,domain=0:720] (pi/180*\x,{cos(1/4*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-cos(1/4*\x)});
\node at (4*pi, 0) [right] {First harmonic $f_0$};
\node at (0, 0) [left] {$l=\frac{\lambda}{2}$};
\end{tikzpicture}

\begin{tikzpicture}[scale=0.5]
\draw (4*pi, -1) -- (0, -1);
\draw (0, 1) -- (4*pi, 1);
\draw plot[smooth,domain=0:720] (pi/180*\x,{cos(1/2*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-cos(1/2*\x)});
\node at (4*pi, 0) [right] {Second harmonic $2f_0$};
\node at (0, 0) [left] {$l=\lambda$};
\end{tikzpicture}

\begin{tikzpicture}[scale=0.5]
\draw (4*pi, -1) -- (0, -1);
\draw (0, 1) -- (4*pi, 1);
\draw plot[smooth,domain=0:720] (pi/180*\x,{cos(3/4*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-cos(3/4*\x)});
\node at (4*pi, 0) [right] {Third harmonic $3f_0$};
\node at (0, 0) [left] {$l=2\lambda$};
\end{tikzpicture}
\end{center}

% -------------------------------------------------------------------
\subsection{Required practical}
% -------------------------------------------------------------------

Investigating stationary waves on strings, specifically how the first
harmonic varies with:\\
 - String length in $m$ ($l$)\\
 - The tension in the string in $N$ ($T$)\\
 - The mass per unit length of string in $kgm^{-1}$ ($\mu$)

It can be shown that the speed of a wave on a string ($v$):\\
$v = \sqrt\frac{T}{\mu}$\\
since $v = f\lambda$\\
$f\lambda = \sqrt\frac{T}{\mu}$\\
$f = \frac{1}{\lambda}\sqrt\frac{T}{\mu}$

To obtain the first harmonic on a string:\\

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.5]
\draw [latex-latex] (0, 1.5) -- (4*pi, 1.5)
  node [above, midway] {$l$};
\draw plot[smooth,domain=0:720] (pi/180*\x,{sin(1/4*\x)});
\draw plot[smooth,domain=0:720] (pi/180*\x,{-sin(1/4*\x)});
\node at (4*pi, 0) [right] {i.e. $\lambda = 2l$};
\end{tikzpicture}
\caption*{
i.e. for the first harmonic:\\
$f = \frac{1}{2l}\sqrt\frac{T}{\mu}$
}
\end{figure}

\paragraph{Example}
If a string of length $0.45m$ under a tension of $80N$ forms a first
harmonic, what if the frequency? [Mass per unit length = $3.2gm^{-1}$]

$3.2gm^{-1}/1000 = 0.0032kgm^{-1}$\\
$f = \frac{1}{2\times0.45}\times\sqrt\frac{80}{0.0032}$

\end{document}

