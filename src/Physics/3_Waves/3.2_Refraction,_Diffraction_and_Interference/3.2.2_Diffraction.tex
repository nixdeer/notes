\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{caption}
\usepackage{}

\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\usepackage{physics}
\usepackage{etoolbox} %ifthen
\usetikzlibrary{calc}

\colorlet{wall}{blue!30!black}
\colorlet{myblue}{blue!70!black}
\colorlet{myred}{red!65!black}
\colorlet{mypurple}{red!50!blue!95!black!75}
\colorlet{myshadow}{blue!30!black!90}
\colorlet{mydarkred}{red!50!black}
\colorlet{mylightgreen}{green!60!black!70}
\colorlet{mygreen}{green!60!black}
\colorlet{myredgrey}{red!50!black!80}
\tikzstyle{wave}=[myblue,thick]
\tikzstyle{mydashed}=[black!70,dashed,thin]
\tikzstyle{mymeas}=[{Latex[length=3,width=2]}-{Latex[length=3,width=2]},thin]
\tikzstyle{mysmallarr}=[-{Latex[length=3,width=2]}]

\tikzset{
  declare function={
    int_arg(\y,\lam,\a,\L) = \a*\y/sqrt(\L*\L+\y*\y)/\lam; %sin(\x);
    int_one(\y,\lam,\a,\L) = (sin(180*int_arg(\y,\lam,\a,\L))/(pi*int_arg(\y,\lam,\a,\L)))^2;
    int_two(\y,\lam,\a,\L) = cos(180*int_arg(\y,\lam,\a,\L))^2;
    int_arg_ang(\t,\lam,\a) = \a*sin(\t)/\lam;
    int_one_ang(\t,\lam,\a) = (sin(180*int_arg_ang(\t,\lam,\a))/(pi*int_arg_ang(\t,\lam,\a)))^2;
    int_two_ang(\t,\lam,\a) = cos(180*int_arg_ang(\t,\lam,\a))^2;
  }
}

\newcommand\rightAngle[4]{
  \pgfmathanglebetweenpoints{\pgfpointanchor{#2}{center}}{\pgfpointanchor{#3}{center}}
  \coordinate (tmpRA) at ($(#2)+(\pgfmathresult+45:#4)$);
  \draw[white,line width=0.6] ($(#2)!(tmpRA)!(#1)$) -- (tmpRA) -- ($(#2)!(tmpRA)!(#3)$);
  \draw[mydarkred] ($(#2)!(tmpRA)!(#1)$) -- (tmpRA) -- ($(#2)!(tmpRA)!(#3)$);
}
\newcommand\lineend[2]{
  \def\w{0.1} \def\c{30}
  \draw[mygreen] (#1)++(#2:\w) to[out=#2-180-\c,in=#2+\c] (#1)
                               to[out=#2+\c-180,in=#2-\c]++ (#2-180:\w);
}
\def\tick#1#2{\draw[thick] (#1) ++ (#2:0.1) --++ (#2-180:0.2)}


% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\begin{document}

% -------------------------------------------------------------------
\section{Diffraction}
% -------------------------------------------------------------------

Diffraction is the spreading out of the wave as it passes through a
gap or around an obstacle.

This effect is best seen with water waves.
\begin{center}
% DIFFRACTION - large slit - circular
\def\H{5.4}     % total wall height
\def\h{4.4}     % plane wave height
\def\t{0.18}    % wall thickness
\def\a{2.3}     % slit size
\def\lambd{0.4} % wavelength
\def\N{14}      % number of waves
\def\dN{30}     % number shift
\def\ang{6}    % angle of diffraction
\begin{tikzpicture}[sca]
  \def\lambd{0.4} % wavelength
  
  % WAVES
  \foreach \i [evaluate={\r=0.9*\i*\lambd;\R=(\i+\dN)*\lambd;}] in {1,...,\N}{
    \ifodd\i
      \draw[blue,line width=0.8] (\r,{\R*sin(\ang)}) arc (\ang:-\ang:\R);
    \else
      \ifnumless{\i}{\N}{
        \draw[blue!80,line width=0.1] (\r,{\R*sin(\ang)}) arc (\ang:-\ang:\R);
      }{}
    \fi
  }
  \foreach \i [evaluate={\xp=-2*(\i-0.5)*\lambd; \xm=-2*(\i-1)*\lambd;}] in {1,...,2}{
    \draw[blue,line width=0.8] (\xp,-\h/2) -- (\xp,\h/2);
    \draw[blue,line width=0.1] (\xm,-\h/2) -- (\xm,\h/2);
  }
  
  % WALL
  \fill[wall]
    (\t/2,\a/2) rectangle (-\t/2,\H/2)
    (\t/2,-\a/2) rectangle (-\t/2,-\H/2);
  
\end{tikzpicture}

% DIFFRACTION - small slit
\begin{tikzpicture}
  \def\H{5.5}     % total wall height
  \def\h{4.0}     % plane wave height
  \def\t{0.18}    % wall thickness
  \def\a{0.68}    % slit distance
  \def\N{7}       % number of waves
  \def\lambd{0.4} % wavelength
  \def\ang{40}
  
  \foreach \i [evaluate={\Rp=2*\i*\lambd; \Rm=2*(\i+0.5)*\lambd;}] in {1,...,\N}{
    \draw[blue,line width=0.8] (-0.9*\lambd,0)++(\ang:\Rp) arc (\ang:-\ang:\Rp);
    \ifnumless{\i}{\N}{
      \draw[blue!80,line width=0.1] (-0.9*\lambd,0)++(\ang:\Rm) arc (\ang:-\ang:\Rm);
    }{}
  }
  \foreach \i [evaluate={\xp=-2*(\i-0.5)*\lambd; \xm=-2*(\i-1)*\lambd;}] in {1,...,2}{
    \draw[blue,line width=0.8] (\xp,-\h/2) -- (\xp,\h/2);
    \draw[blue,line width=0.1] (\xm,-\h/2) -- (\xm,\h/2);
  }
  \fill[black]
    (\t/2,\a/2) rectangle (-\t/2,\H/2)
    (\t/2,-\a/2) rectangle (-\t/2,-\H/2);
\end{tikzpicture}
\end{center}

The closer the gap in the obstacle to the wavelength of the wave, the
more the wave diffracts.

The amount a wave is diffracted is based on it's wavelength.

\paragraph{For Example}
Sound waves, travelling at $330ms^{-1}$ with a frequency of $330Hz$,
has a wavelength of $\frac{330ms^{-1}}{330Hz} = 1m$. This means that
these sound waves will maximally diffract through a gap of about $1m$.

% -------------------------------------------------------------------
\subsection{Diffraction Grating}
% -------------------------------------------------------------------

Diffraction grating has multiple slits, which create more complex
interference patterns.

Similarly with a double slit, with a diffraction grating, we get
constructive interference only when the path difference between any
two rays is a whole integer number of wavelengths. I.e, we see a
bright spot if
\begin{center}
\LARGE
$d sin\Theta = n \lambda$
\end{center}

When $\Theta = 0$, there is a bright spot.\\
When $n = 1$, we call this the first order beam\\
When $n = 2$, we call this the second order beam\\
and so on ...

The maximum number of orders for a wavelength can be derived as so:
\begin{center}
\LARGE
max of $sin\Theta = 1$\\
$d = n_{max} \times \lambda$\\
$n_{max} = \frac{\lambda}{d}$
\end{center}

% -------------------------------------------------------------------
\subsection{Single Slit Diffraction}
% -------------------------------------------------------------------

Single slit diffraction occurs because each point in the slit can be
considered as a seperate source of a wave.

It can be shown that:
\begin{center}
\text{\LARGE{$sin\Theta = \frac{\lambda}{a}\times 2D$}}

for the angle of the first minima from the source\\
where $\lambda = $ wavelength, $a = $ slit width and $D = $ distance
from screen.
\end{center}



\end{document}

