\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\usepackage[a4paper, margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{bohr}
\usepackage{caption}

\usetikzlibrary{intersections}
\usetikzlibrary{scopes}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\begin{document}

% -------------------------------------------------------------------
\section{Reflection}
% -------------------------------------------------------------------

In 1680, a physicist called Chrisiaan Huygens formulated the idea of
light behaving like a wave, rather than a simple ray. These ideas
allowed Huygens to explain many of the complex behaviours of light,
such as refraction.

% -------------------------------------------------------------------
\subsection{Internal Reflection}
% -------------------------------------------------------------------

A ray of light can reflect at the boundary of an optically dense and
optically less dense medium. 

% -------------------------------------------------------------------
\subsection{Total Internal Reflection}
% -------------------------------------------------------------------

When the ray of light is inside a optically denser medium, it  can be
reflected back inside. This is referred to as total internal
reflection. This occurs only the ray is inside an optically denser
medium, and the angle of incidence is greater than the critical
angle.

\begin{figure}[h]
\centering
\begin{tikzpicture}
\draw (-5, 0) -- (5, 0);
\draw[dashed] (0, 1.5) -- (0, -1.5);
\draw (-2, -3) -- (0, 0);
\draw (2, -3) -- (0, 0);
\draw (0, 0) -- (4, 2);
\draw[red] (-3, -3) -- (0, 0);
\draw[red] (0, 0) -- (0, 0);
\draw[blue] (-4, -3) -- (0, 0);
\end{tikzpicture}
\caption*{
critical angle = $n_{1}\sin\Theta_{1} = n_{2}\sin\Theta_{2}$\\
$n_{1}\sin\Theta_{1} = n_{2}\sin90$\\
$\sin\Theta_{c} = \frac{n_{2}}{n_{1}}$
}
\end{figure}

% -------------------------------------------------------------------
\subsection{Fiber Optics}
% -------------------------------------------------------------------

\begin{figure}[h]
\centering
\begin{tikzpicture}
\draw (-8, 1.2) -- (8, 1.2)
  node[below, midway] {\footnotesize{cladding}};
\draw (-8, 0.6) -- (8, 0.6)
  node[below, midway] {\footnotesize{core}};
\draw (-8, -0.6) -- (8, -0.6);
\draw (-8, -1.2) -- (8, -1.2);
{ [dashed]
  \draw (4, 1) -- (4, 0.2);
  \draw (0, -1) -- (0, -0.2);
  \draw (-4, 1) -- (-4, 0.2);
}
{ [red]
  \draw (-8, -0.6) -- (-4, 0.6);
  \draw (-4, 0.6) -- (0, -0.6);
  \draw (0, -0.6) -- (4, 0.6);
  \draw (4, 0.6) -- (8, -0.6);
}
{
  \clip (-4, 0.6) -- (0, -0.6) -- (-4, -0.6) -- cycle;
  \node [draw, minimum size=0.6cm, circle] at (-4, 0.6) {};
}
\node at (-3.6, 0.2) {$\Theta_c$};
\end{tikzpicture}
\caption*{
  The cores of fiber optic cables are normally made of glass for long
  distance transmissions, while plastics are used for shorter
  distance transmissions.
}
\end{figure}

Advantages\\
 - More information carried per second\\
 - Cheaper (than copper cabling)\\
 - Less suceptible to interference\\
 - Signal travels further, less signal loss\\

% -------------------------------------------------------------------
\section{Refraction}
% -------------------------------------------------------------------

Refraction is caused by a wave changing speed as it enters or leaves
a medium of different optical density.
\begin{figure}[h]
\centering
\begin{tikzpicture}
  \draw (-5, 0) -- (5, 0);
  {
    \clip[overlay] (-5, 5) rectangle (5, 0);
    \draw[rotate=30, overlay=false]
      (-2, -2) edge ++(0, 6)
      (2,  -2) edge ++(0, 6)
      (0.1, 1.65) edge[blue] (0, 1.5)
      (0, 1.5) edge[blue] (-0.1, 1.65)
      (0, -2) edge[blue] ++(0, 6)
	  (-2, 1.4) -- (-1.75, 1.4) -- (-1.75, 1.15) 
      (-2, 1.15) edge[dashed] (2, 1.15);
  }
  {
    \clip[overlay] (-5, -5) rectangle (5, 0);
    \pgfmathsetmacro{\myx}{2*cos(20)/cos(30)};
    \draw[rotate=200, overlay=false]
	  (-\myx, -2) edge ++(0, 6)
	  (\myx, -2) edge ++(0, 6)
      (0.1, 1.35) edge[blue] (0, 1.5)
      (0, 1.5) edge[blue] (-0.1, 1.35)
      (0, -2) edge[blue] ++(0, 6)
	  (\myx, 0.55) -- (\myx-0.25, 0.55) -- (\myx-0.25, 0.8) 
      (\myx, 0.8) edge[dashed] (-\myx, 0.8);
  }
  \draw[dashed] (-2.31, 1.5) -- (-2.31, -1.5)
    (2.31,  1.5) -- (2.31,  -1.5);
  {
    \path [clip] (2.31, 1.5) -- (2.31, 0) -- (1.55, 1.3) -- cycle;
    \node [draw, red, minimum size=2cm, circle] at (2.31, 0) {};
  }
  {
    \path [clip] (-2.31, -1.5) -- (-2.31, 0) -- (-1.8, -1.4) -- cycle;
    \node [draw, red, minimum size=2cm, circle] at (-2.31, 0) {};
  }
  \node at (2, 1.3) {$\Theta_1$};
  \node at (-2, -1.5) {$\Theta_2$};
  \node at (-4.1, 0.3) {$\text{medium}_1$};
  \node at (-4.1, -0.3) {$\text{medium}_2$};
\end{tikzpicture}

\LARGE
$\frac{\sin\Theta_1}{\sin\Theta_2} = \frac{c\text{ in medium}_1}{c\text{ in medium}_2}$
\caption*{Refractive index of a system according to Snell's Law}
\end{figure}

The refractive index of a medium (also called the absolute
refractive index) is defined by:
\begin{center}
\large
$\text{Refractive Index} (n) = \frac{\text{Speed of light in vacuum}}
{\text{Speed of light in medium}}$
\end{center}

"Relative" refractive index can also be expressed as:
\begin{center}
\LARGE
$_1n_2 = \frac{c_1}{c_2} = \frac{\sin\Theta_1}{\sin\Theta_2}$\\
$_2n_1 = \frac{1}{_1n_2}$

$n_1 \sin\Theta_1 = n_2 \sin\Theta_2$
\end{center}

\end{document}

