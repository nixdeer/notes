#!/bin/sh

find . -iname "*.pdf" | while read file; do
  cp "$file" "$(readlink -f "$(dirname "$file")" | sed 's|/src/|/out/|')"
  if test "$?" = 0; then
    echo "Copied $file"
  fi
done
