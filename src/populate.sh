#!/bin/sh
IFS='\n'
find . -iname "*.tex" | while read file; do
  if test "$(md5sum "$file" | awk '{ print $1; }')" = "$1"; then
  echo '\\documentclass[a4paper, 12pt]{article}

% -------------------------------------------------------------------
% Packages & Configs
% -------------------------------------------------------------------

\\usepackage[a4paper, margin=0.5in]{geometry}
\\usepackage{graphicx}
\\usepackage{mathtools}
\\usepackage{mhchem}
\\usepackage{chemfig}
\\usepackage{caption}

\\usetikzlibrary{scopes}

\\setlength{\parindent}{0em}
\\setlength{\parskip}{1em}

% -------------------------------------------------------------------
% Document
% -------------------------------------------------------------------

\\begin{document}

% -------------------------------------------------------------------
\\section{Section}
% -------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

% -------------------------------------------------------------------
\\subsection{Subsection}
% -------------------------------------------------------------------

\\end{document}
' > $file
echo "$file"
fi
done